<?php
require '../vendor/autoload.php';

use Calculator\Sum as Addition;
use Calculator\helloworld\hello as hello;
use Noodlehaus\Config;

/**
*
*/
    $add = new Addition;
    $add->add(1, 1);
    $add->add(null,null);

    $sub = new Addition;
    $sub->sub(1, 1);
    //$sub->sub(null,null);


    $cos = new Addition;
    $cos->cos(2);
    $cos->cos(null);

    $div = new Addition;
    $div->divide(2,2);
    //$div->divide(2,0);

    $var = date('H:i');
    $hello = new Hello;
    $hello->greeting($var);

// Load a single file
$conf = Config::load('../vendor/hassankhan/config/composer.json');
$conf = new Config('../vendor/hassankhan/config/composer.json');
// Get value using key
$debug = $conf->get('debug');
// Get all values
$data = $conf->all();
