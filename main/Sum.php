<?php
namespace Calculator;
class Sum
{
    
    public function add($x, $y)
    {
        if((gettype($x)=='integer' && gettype($y)=='integer') || (gettype($x)=='double' && gettype($y)=='double')) 
        {
            $sum=$x+$y;
            echo  "$sum<br>\n";
        	return $sum; 
        }
        else{
            throw new \InvalidArgumentException("Not integers", 1);
            
        }         
    }
    public function sub($x, $y)
    {
        if((gettype($x)=='integer' && gettype($y)=='integer') || (gettype($x)=='double' && gettype($y)=='double')) 
        {
            $sum=$x-$y;
            echo  "$sum<br>\n";
    	   return $sum;
        }
        else{
            throw new \InvalidArgumentException("Not integers", 1);
            
        }   
    }
    public function cos($x)
    {
        $sum=cos($x);
        echo  "$sum\n<br>";
	return $sum;
    }
    public function divide($x,$y)
    {
	if($y<>0)
	{
        $sum=$x/$y;
        echo  "$sum\n<br>";
        return $sum;
    }
    else{
            throw new \InvalidArgumentException("Cannot divide by zero", 1);
            
        }
    }
}
