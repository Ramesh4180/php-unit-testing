<?php
namespace Test;
//require '../../vendor/autoload.php';
/**
*
*/
class Arithmetic extends \PHPUnit_Framework_TestCase
{
    public function testCanAddTwoPositiveNumbers( )
    {
        $c = new \Calculator\Sum();

        $result = $c->add(2, 2);
        
        $this->assertEquals(4, $result);
    }
    public function testCanAddTwoPositiveFloatNumbers( )
    {
        $c = new \Calculator\Sum();

        $result = $c->add(2.8, 2.8);
        
        $this->assertEquals(5.6, $result);
    }
    public function testCanAddOnePositiveOneNegativeNumbers( )
    {
        $c = new \Calculator\Sum();

        $result = $c->add(2, -2);
        
        $this->assertEquals(0, $result);
    }
    public function testCanSubtractTwoFloatNumbers( )
    {
        $operation = new \Calculator\Sum();

        $result = $operation->sub(2.2, 2.2);

        $this->assertEquals(0, $result);
    }
    public function testCanSubtractTwoNumbers( )
    {
        $operation = new \Calculator\Sum();

        $result = $operation->sub(2, 2);

        $this->assertEquals(0, $result);
    }
    public function testCanSubtractTwoNegativeNumbers( )
    {
        $operation = new \Calculator\Sum();

        $result = $operation->sub(-2, -2);

        $this->assertEquals(0, $result);
    }
    public function testCanSubtractOneNegativeOnePositiveNumbers( )
    {
        $operation = new \Calculator\Sum();

        $result = $operation->sub(-2, 2);

        $this->assertEquals(-4, $result);
    }
    public function testGivesCosValue( )
    {
        $operation = new \Calculator\Sum();

        $result = $operation->cos(2);

        $this->assertEquals(-0.41614683654, $result);
    }
    public function testDividesTwoPositiveNumbers( )
    {
        $operation = new \Calculator\Sum();

        $result = $operation->divide(2,2);

        $this->assertEquals(1, $result);
    }
    public function testDividesTwoNegativeNumber( )
    {
        $operation = new \Calculator\Sum();

        $result = $operation->divide(-2,-2);

        $this->assertEquals(1, $result);
    }
    public function testDividesOnePositiveOneNegativeNumber( )
    {
        $operation = new \Calculator\Sum();

        $result = $operation->divide(2,-2);

        $this->assertEquals(-1, $result);
    }
   /**
     * @expectedException InvalidArgumentException
     */
    
    public function testDividesTwoNumbersWithSecondNumberZero( )
    {
        $operation = new \Calculator\Sum();

        $result = $operation->divide(2,0);

        echo "Inside Exception";

    }
    /**
     * @expectedException InvalidArgumentException
     */
    
    public function testForAnyThingOtherThanNumeric( )
    {
        $operation = new \Calculator\Sum();

        $result = $operation->add(null,null);

        $result1 = $operation->add(one,two);

        $result2 = $operation->sub(null,null);

        $result3 = $operation->sub(one,two);

        $result4 = $operation->cos(null);

        echo "Inside Exception";

    }
}
